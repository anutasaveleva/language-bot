﻿using System;
using System.Net;
using System.Threading.Tasks;

namespace Application
{
    class HttpListen
    {
        private static readonly HttpListener Listener = new HttpListener();

        public static void Main()
        {
            Listener.Prefixes.Add($"http://localhost:8888/");
            Listener.Start();
            Listen();
            Console.WriteLine("Listening...");
        }

        private static void Listen()
        {
            while (true)
            {
                var context = Listener.GetContext();
                Console.WriteLine("Client connected");
                Task.Factory.StartNew(() => ProcessRequest(context));
            }
            
        }

        private static void ProcessRequest(HttpListenerContext context)
        {
            var tokens = context.Request.Url.AbsolutePath.TrimStart('/').Split('/');

            Console.WriteLine($"Response sent: {tokens}");
        }
        
    }
}
