﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LibWorkPlease
{
    public class Infrastructure
    {
        static Random r = new Random();
        static int count;

        public static Word GetNewWord(List<int> wordid = null)
        {
            Word foundWord = new Word();
            using (ApplicationContext db = new ApplicationContext())
            {
                if (wordid != null)
                {
                    var cap = wordid.Count;
                    if (cap > 0)
                    {
                        var wordIdx = wordid[cap - 1];
                        return db.Words.Find(wordIdx);
                    }
                    else return foundWord;
                }
                count = db.Count;
                do
                {
                    var n = r.Next(count) + 1;
                    foundWord = db.Words.Find(n);
                } while (foundWord == null);
                db.SaveChanges();
            }
            return foundWord;
        }

        public static State Start(long chatId, string message)
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                if (db.Users.Find(chatId) == null)
                    db.Users.Add(new User(chatId, State.Start, GetNewWord().WordId));
                db.SaveChanges();
            }
            if (message.Equals("Learn"))
                return State.Slearning;
            if (message.Equals("Create"))
                return State.Screating;
            return State.Start;
        }

        public static State ChooseWayOfLearning(long chatId, string msg)
        {
            if (msg.Equals("Random"))
                return State.Learning;
            else if (msg.Equals("From a topic"))
                return State.ChoosingTopic;
            return State.Error;

        }
        public static Word ReduceProgress(long chatid, Word foundWord)
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                var pr = db.UserHasProgress
                    .Where(x => x.ChatId == chatid && x.WordId == foundWord.WordId)
                    .SingleOrDefault();
                if (pr == null)
                    return foundWord;
                if (pr.ProgressRate > 0)
                    pr.ProgressRate--;
                else
                {
                    var progress = new Progress(chatid, foundWord.WordId);
                    db.Users
                        .Find(progress.ChatId)?
                        .WordsProgress
                        .Add(progress);
                }
                db.SaveChanges();
            }
            return foundWord;
        }

        public static Word GetProgress(long chatid, Word foundWord)
        {
            using (ApplicationContext db = new ApplicationContext())
            {

                var usp = db.UserHasProgress
                    .Where(x => x.ChatId == chatid && x.WordId == foundWord.WordId)
                    .SingleOrDefault();

                if (usp == null)
                {
                    db.Users.Find(chatid).WordsProgress.Add(new Progress (chatid, foundWord.WordId));
                    db.SaveChanges();
                }
                else if (usp.ProgressRate < 5)
                    usp.ProgressRate++;
                db.SaveChanges();
            }
            return foundWord;
        }

        public static List<int> FindTopic(Topic topic, long chatid)
        {
            var r = new Random();
            var uTopic = new UserTopic(chatid, topic.TopicId);
            using (ApplicationContext db = new ApplicationContext())
            {
                db.Users.Find(chatid).LearningTopics.Add(uTopic);
                db.Users.Find(chatid).CurTopicID = topic.TopicId;
                db.SaveChanges();
                var l = db.WordInTopic
                    .Where(x => x.TopicId == topic.TopicId)
                    .Select(x => x.WordId)
                    .ToList();
                return l;
            }
        }

        public static State CreateTopic(string name, long chatid)
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                var topic = new Topic(name);
                var t = "";
                foreach (var i in db.Topics)
                    if (i.Name == name)
                        t = i.Name;
                if (t == "")
                    db.Topics.Add(topic);
                else return State.Screating;
                db.SaveChanges();
                db.Users.Find(chatid).CurTopicID = topic.TopicId;
                db.SaveChanges();
            }
            return State.Adding;
        }

        public static State AddWord(string message, int topicid)
        {
            if (message == "Done")
                return State.Created;
            else if (message == "Cancel")
                return 0;
            else
            {
                var arr = message.Split(' ');
                using (ApplicationContext db = new ApplicationContext())
                {
                    var c = new Word() { Eng_version = arr[0], Rus_version = arr[1] };
                    db.Words.Add(c);
                    db.SaveChanges();
                    WordTopic wt = new WordTopic();
                    wt.TopicId = topicid;
                    wt.WordId = c.WordId;
                    db.Topics.Find(topicid).WordsInTopic.Add(wt);
                    db.SaveChanges();
                }
            }
            return State.Adding;
        }
    }
}