﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LibWorkPlease
{
    public enum State
    {
        Start = 1,
        Screating = 2,
        Adding = 3,
        Created = 4,
        Slearning = 5,
        Learning = 6,
        ChoosingTopic = 7,
        Error = 8,
        LearningTopic = 9,
        Learned = 10
    }
}
