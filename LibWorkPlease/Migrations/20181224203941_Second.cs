﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LibWorkPlease.Migrations
{
    public partial class Second : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<List<int>>(
                name: "CurTopic",
                table: "Users",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CurTopic",
                table: "Users");
        }
    }
}
