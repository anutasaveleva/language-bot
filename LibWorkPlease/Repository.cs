﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibWorkPlease
{
    public class Repository
    {
        private ApplicationContext context;
        public Repository()
        {
            context = new ApplicationContext();
        }

        public bool ContainUser(long userId) => 
            context
            .Users
            .Where(u => u.ChatId == userId)
            .Any();
        public User GetUser(long chatid) => context.Users.Find(chatid);
        public Word GetWord(int wordid) => context.Words.Find(wordid);
        public List<int> GetWords(int topicid)
        {
            return context
            .WordInTopic.Where(x => x.TopicId == topicid)
            .Select(x => x.WordId)
            .ToList();
        }

        public Topic FindTopic(string name) =>
             context
            .Topics
            .Select(x => x)
            .Where(x => x.Name == name)
            .FirstOrDefault();
        public Progress ReturnProgress(long chatid, int wordid) => 
            context
            .UserHasProgress
            .Where(x => x.ChatId == chatid && x.WordId == wordid)
            .SingleOrDefault();

        public void AddUser(long userId, State curState)
        {
            var context = new ApplicationContext();
            context.Users.Add(new User(userId, curState));
            context.SaveChanges();
        }
        public void AddUser(long userId, State curState, int wordid)
        {
            var context = new ApplicationContext();
            context.Users.Add(new User(userId, curState, wordid));
            context.SaveChanges();
        }
        public void ChangeState(long userId, State curState)
        {
            GetUser(userId).UState = curState;
            context.SaveChanges();
        }
        
        public void AddTopic(UserTopic topic, long chatId)
        {
            var user = context.Users.Find(chatId);
            user.LearningTopics.Add(topic);
            user.CurTopicID = topic.TopicId;
            context.SaveChanges();
        }

        public void AddTopic(Topic topic)
        {
            context.Topics.Add(topic);
        }
        public void IncreaseRate(Progress pr)  {
        pr.ProgressRate++;
            context.SaveChanges();
 }
        public void AddProgress(long chatid, Progress pr)
        {
            GetUser(chatid).WordsProgress.Add(pr);
            context.SaveChanges();
        }
        private int a;
        public int Count
        {
            get
            {
                GetCount().Wait();
                return a;
            }
        }
        private async Task GetCount() => a = await context.Words.CountAsync();

        public List<int> CreateTopic(long chatid, int topicid)
        {
            return context.WordInTopic
                                    .Where(x => x.TopicId == topicid)
                                    .Select(x => x.WordId)
                                    .ToList();
        }

        public void UpdateUser(long chatId, User updatedVersion)
        {
            var user = GetUser(chatId);
            user = updatedVersion;
            context.SaveChanges();
        }
    }
}
