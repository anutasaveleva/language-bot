﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LibWorkPlease
{
    public class Topic
    {
        public int TopicId { get; set; }
        public string Name { get; set; }

        public List<UserTopic> LearningBy { get; set; }
        public List<WordTopic> WordsInTopic { get; set; }
        public Topic()
        {
            WordsInTopic = new List<WordTopic>();
            LearningBy = new List<UserTopic>();
        }

        public Topic(string name)
        {
            Name = name;
            WordsInTopic = new List<WordTopic>();
            LearningBy = new List<UserTopic>();
        }
    }
}
