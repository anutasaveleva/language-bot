﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace LibWorkPlease
{

    public class User
    {
        [Key]
        public long ChatId { get; set; }
        public State UState { get; set; }
        public List<int> CurTopic { get; set; }
        public int CurrentWord { get; set; }
        public int CurTopicID { get; set; }
        public List<UserTopic> LearningTopics { get; set; }
        public List<Progress> WordsProgress { get; set; }

        public User()
        {
            LearningTopics = new List<UserTopic>();
            WordsProgress = new List<Progress>();
        }

        public User(long chatid, State state)
        {
            LearningTopics = new List<UserTopic>();
            WordsProgress = new List<Progress>();
            ChatId = chatid;
            UState = state;
            CurTopic = new List<int>();
        }

        public User(long chatid, State state, int wordid)
        {
            LearningTopics = new List<UserTopic>();
            WordsProgress = new List<Progress>();
            ChatId = chatid;
            UState = state;
            CurrentWord = wordid;
            CurTopic = new List<int>();
        }
    }
}
