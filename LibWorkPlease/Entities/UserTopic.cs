﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace LibWorkPlease
{
    public class UserTopic
    {
        public int UserTopicId { get; set; }
        public long ChatId { get; set; }
        public int TopicId { get; set; }
        public bool IsLearned { get; set; }

        public Topic Topic { get; set; }
        public User User { get; set; }

        public UserTopic(long chatid, int topicid)
        {
            ChatId = chatid;
            TopicId = topicid;
            IsLearned = false;
        }
        public UserTopic() { }
    }
}
