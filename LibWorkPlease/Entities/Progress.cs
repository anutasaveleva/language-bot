﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace LibWorkPlease
{
    public class Progress
    {
        public int ProgressId { get; set; }
        public long ChatId { get; set; }
        public int WordId { get; set; }
        [Range(0, 5)]
        public int ProgressRate { get; set; }

        public User User { get; set; }
        public Word Word { get; set; }

        public Progress(long chatid, int wordid)
        {
            ChatId = chatid;
            WordId = wordid;
            ProgressRate = 0;
        }

        public Progress() { }
    }
}
