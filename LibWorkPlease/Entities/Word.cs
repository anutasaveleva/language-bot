﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LibWorkPlease
{
    public class Word
    {
        public int WordId { get; set; }
        public string Rus_version { get; set; }
        public string Eng_version { get; set; }

        public List<WordTopic> InTopics { get; set; }
        public List<Progress> LearnBy { get; set; }
        public Word()
        {
            LearnBy = new List<Progress>();
            InTopics = new List<WordTopic>();
        }
        public Word(int wordid, string rus, string eng)
        {
            WordId = wordid;
            Rus_version = rus;
            Eng_version = eng;
        }
    }
}
