﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LibWorkPlease
{
    public class WordTopic
    {
        public int WordTopicId { get; set; }
        public int TopicId { get; set; }
        public int WordId { get; set; }

        public Word Word { get; set; }
        public Topic Topic { get; set; }
    }
}
