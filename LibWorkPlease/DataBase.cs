﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
using Npgsql;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Threading.Tasks;
using System.Text;
using System.IO;
using OfficeOpenXml;

namespace LibWorkPlease
{
    public class ApplicationContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Topic> Topics { get; set; }
        public DbSet<Word> Words { get; set; }
        public DbSet<Progress> UserHasProgress { get; set; }
        public DbSet<WordTopic> WordInTopic { get; set; }
        public DbSet<UserTopic> UserLearnTopic { get; set; }

        public ApplicationContext()
        {
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql("Host=ec2-54-75-231-3.eu-west-1.compute.amazonaws.com;" +
                "Port=5432;Database=d8tt7cu0qfu922;" +
                "Username=yoydtjjgtntyfw;" +
                "Password=55cc4f509bd8423464355a1dd27181b6945b9e2307ad510eb21f04716d04f964;SslMode=Require;Trust Server Certificate=True");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
             .HasKey(t => t.ChatId);
            modelBuilder.Entity<Topic>()
             .HasKey(t => t.TopicId);
            modelBuilder.Entity<Topic>()
             .HasIndex(t => t.Name);
            modelBuilder.Entity<Word>()
             .HasKey(t => t.WordId);
            modelBuilder.Entity<Progress>()
             .HasKey(t => t.ProgressId);
            modelBuilder.Entity<WordTopic>()
             .HasKey(t => t.WordTopicId);
            modelBuilder.Entity<UserTopic>()
             .HasKey(t => t.UserTopicId);
        }

        private int a;
        public int Count
        {
            get
            {
                var t = GetCount();
                t.Wait();
                return a;
            }
        }
        private async Task GetCount() => a = await Words.CountAsync();
    }

    public class Prog
    {
        private static void UpdateDB()
        {
            var filepath = @"H:/dictionary.xlsx";
            FileInfo file = new FileInfo(filepath);
            using (ApplicationContext db = new ApplicationContext())
            {
                using (ExcelPackage package = new ExcelPackage(file))
                {
                    StringBuilder sb = new StringBuilder();
                    ExcelWorksheet worksheet = package.Workbook.Worksheets[0];
                    int rowsNum = worksheet.Dimension.Rows;
                    int colNum = worksheet.Dimension.Columns;
                    for (int row = 2; row <= rowsNum; row++)
                    {
                        Word word1 = new Word();
                        for (int col = 2; col <= 3; col++)
                        {
                            var w = worksheet.Cells[row, col].Value.ToString();
                            if (col % 2 == 1)
                            {
                                w = w.Split(new[] { ',', ';' })[0];
                                word1.Eng_version = w;
                            }
                            else word1.Rus_version = w;
                        }
                        db.Words.Add(word1);
                        db.SaveChanges();
                    }
                }

            }
        }

        static void Main()
        {
            using (ApplicationContext db = new ApplicationContext())
            {

                foreach (var u in db.Users)
                {
                    Console.WriteLine($"{u.ChatId} {u.UState} {u.CurrentWord}");
                    foreach(var p in u.WordsProgress)
                        Console.WriteLine($"{p.ChatId} {p.WordId} {p.ProgressRate}");
                    
                }
                foreach (var u in db.Topics)
                    Console.WriteLine($"{u.TopicId} {u.WordsInTopic} ");

            }
        }
    }
}
