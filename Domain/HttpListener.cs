﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using Newtonsoft.Json;

namespace Domain
{
    public class Listener
    {

        private readonly HttpListener httpListener;
        private readonly Func<Server> _serverProvider;
        private readonly string _prefix;

        private void Listen()
        {
            httpListener.Prefixes.Add("http://localhost:8888/");
            httpListener.Start();
            Console.WriteLine($"Now listening on {_prefix}");
            try
            {
                while (true)
                {
                    var context = httpListener.GetContext();

                    Task.Run(() =>
                    {
                        var result = _serverProvider().HandleRequest(context);

                        var jsonResult = JsonConvert.SerializeObject(result);
                        using (var output = new StreamWriter(context.Response.OutputStream))
                            output.Write(jsonResult);
                    });
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"{DateTime.UtcNow}: {e}");
            }
        }
    }

    public class Server
    {

        public object HandleRequest(HttpListenerContext context)
        {
            try
            {
                return context.Request.QueryString["query"] == null ? 0:1
                    ;
            }
            catch (Exception exception)
            {
                context.Response.StatusCode = (int)HttpStatusCode.BadRequest;

                Console.WriteLine($"{DateTime.UtcNow}: {exception}");
                return exception.Message;
            }
        }
    }
    }
