﻿using com.LandonKey.SocksWebProxy;
using com.LandonKey.SocksWebProxy.Proxy;
using Microsoft.Extensions.Configuration;
using LibWorkPlease;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Telegram.Bot;

namespace LanBot
{
    public static class Program
    {
        private static void Main(string[] args)
        {
            var bots = CreateBots();
            StartBots(bots);

            while (true)
            {
                var command = Console.ReadLine();
                if (command == "stop")
                    return;
            }
        }

        private static IEnumerable<ITelegramBotClient> CreateBots()
        {
            yield return GetSOCK5Proxy();
        }

        private static ITelegramBotClient GetSOCK5Proxy()
        {
            var proxy = new SocksWebProxy(
                new ProxyConfig(
                    IPAddress.Parse("127.0.0.1"),
                    GetNextFreePort(),
                    IPAddress.Parse("185.20.184.217"),
                    3693,
                    ProxyConfig.SocksVersion.Five,
                    "userid66n9", "pSnEA7M"),
                false);
            
            var bc = new TelegramBotClient("651182684:AAGZhj1BO-t-Bz1Q38QVuXljx-Dg44Y0Yqw", proxy);
            bc.SetWebhookAsync("//https://bot.skynet-kazan.com/api/webhook");
            Console.WriteLine("Created BotClient with SOCKS5 Proxy.");
            return bc;
        }

        private static int GetNextFreePort()
        {
            var listener = new System.Net.Sockets.TcpListener(IPAddress.Loopback, 0);
            listener.Start();
            var port = ((IPEndPoint)listener.LocalEndpoint).Port;
            listener.Stop();

            return port;
        }

        private static void StartBots(IEnumerable<ITelegramBotClient> bots)
        {
            foreach (var bot in bots)
            {
                bot.OnMessage += LanBot.Bot.Bot_OnMessage;
                Task.Run(() => bot.StartReceiving());
                Console.WriteLine($"Started bot with id {bot.BotId}");
            }
        }
    }
}