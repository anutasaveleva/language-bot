﻿using LibWorkPlease;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types.ReplyMarkups;

namespace LanBot
{
    internal class Helpers
    {
        internal static async Task<ReplyKeyboardMarkup> LearnCreateRKM(MessageEventArgs mea, ITelegramBotClient botClient)
        {
            ReplyKeyboardMarkup rk = MakeRKM(mea, "Learn", "Create");
            await botClient.SendTextMessageAsync(
                chatId: mea.Message.Chat,
                text: "Do you want to learn or create your own list?",
                replyMarkup: rk);
            return rk;
        }

        internal static async Task WordCheck(MessageEventArgs mea, long chatid, ITelegramBotClient botClient)
        {
            Word wor;
            List<int> wordIds;
            using (ApplicationContext db = new ApplicationContext())
            {
                var user = db.Users.Find(chatid);
                var w = user.CurrentWord;
                wor = db.Words.Find(w);
                wordIds = user.CurTopic;


                if (mea.Message.Text.ToLower().Equals(wor.Eng_version.ToLower()))
                {
                    wor = Infrastructure.GetProgress(chatid, wor);
                    if (wordIds?.Count > 0)
                        wordIds = wordIds.SkipLast(1).ToList();
                    if (wordIds.Count == 0)
                    {
                        user.CurTopic = null;
                        wordIds = null;
                        db.SaveChanges();
                        user.UState = State.Start;
                        user.CurTopic = wordIds;
                        await botClient.SendTextMessageAsync(
                            chatId: mea.Message.Chat,
                            text: "The topic is learned!");

                        db.SaveChanges();
                        await LearnCreateRKM(mea, botClient);
                        return;
                    }
                    wor = Infrastructure.GetNewWord(wordIds);
                    user.CurTopic = wordIds;
                    db.SaveChanges();

                    await botClient.SendTextMessageAsync(
                        chatId: mea.Message.Chat,
                        text: wor.Rus_version);
                }

                else if (mea.Message.Text == "I don't know!")
                {
                    wor = Infrastructure.ReduceProgress(chatid, wor);
                    await botClient.SendTextMessageAsync(
                        chatId: mea.Message.Chat,
                        text: $"Try to remember it this time: {wor.Eng_version}");
                    wor = Infrastructure.GetNewWord();
                    await botClient.SendTextMessageAsync(
                        chatId: mea.Message.Chat,
                        text: wor.Rus_version);
                }

                else
                {
                    wor = Infrastructure.ReduceProgress(chatid, wor);
                    var rk = MakeRKM(mea, "I don't know!", "Cancel");

                    await botClient.SendTextMessageAsync(
                        chatId: mea.Message.Chat,
                        text: $"The translation is wrong. Try again: {wor.Rus_version}", replyMarkup: rk);
                }
            }
            using (ApplicationContext db = new ApplicationContext())
            {
                db.Users.Find(chatid).CurrentWord = wor.WordId;
                db.SaveChanges();
            }
        }

        internal static async Task WordCheckS(MessageEventArgs mea, long chatid, ITelegramBotClient botClient)
        {
            Word wor;
            using (ApplicationContext db = new ApplicationContext())
            {
                var user = db.Users.Find(chatid);
                var w = user.CurrentWord;
                wor = db.Words.Find(w);


                if (mea.Message.Text.ToLower().Equals(wor.Eng_version.ToLower()))
                {
                    wor = Infrastructure.GetProgress(chatid, wor);
                    wor = Infrastructure.GetNewWord();

                    await botClient.SendTextMessageAsync(
                        chatId: mea.Message.Chat,
                        text: wor.Rus_version);
                }

                else if (mea.Message.Text == "I don't know!")
                {
                    wor = Infrastructure.ReduceProgress(chatid, wor);
                    await botClient.SendTextMessageAsync(
                        chatId: mea.Message.Chat,
                        text: $"Try to remember it this time: {wor.Eng_version}");
                    wor = Infrastructure.GetNewWord();
                    await botClient.SendTextMessageAsync(
                        chatId: mea.Message.Chat,
                        text: wor.Rus_version);
                }

                else
                {
                    wor = Infrastructure.ReduceProgress(chatid, wor);
                    var rk = MakeRKM(mea, "I don't know!", "Cancel");

                    await botClient.SendTextMessageAsync(
                        chatId: mea.Message.Chat,
                        text: $"The translation is wrong. Try again: {wor.Rus_version}", replyMarkup: rk);
                }
            }
            using (ApplicationContext db = new ApplicationContext())
            {
                db.Users.Find(chatid).CurrentWord = wor.WordId;
                db.SaveChanges();
            }
        }

        internal static ReplyKeyboardMarkup MakeRKM(MessageEventArgs mea, string m1, string m2, string q = "")
        {
            var rkm = new ReplyKeyboardMarkup();
            var p = new List<KeyboardButton[]>();
            var answ = new List<KeyboardButton>();
            rkm.OneTimeKeyboard = true;
            rkm.ResizeKeyboard = true;
            answ.Add(new KeyboardButton(m1));
            answ.Add(new KeyboardButton(m2));
            p.Add(answ.ToArray());
            rkm.Keyboard = p;
            return rkm;
        }

        internal static string SendRequest(long chatId, State state, string mes = "default")
        {
            try
            {
                var request = WebRequest.Create($"http://localhost:8888/{(int)state}/{mes}/{chatId}");
                var response = request.GetResponse();
                var responseStream = response.GetResponseStream();

                using (var reader = new StreamReader(responseStream))
                {
                    var content = reader.ReadToEnd();
                    return content;
                }
            }
            catch (Exception e)
            { Console.WriteLine(e); }
            return "";
        }
    }
}
