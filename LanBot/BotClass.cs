﻿using com.LandonKey.SocksWebProxy;
using com.LandonKey.SocksWebProxy.Proxy;
using System;
using System.IO;
using System.Net;
using System.Threading;
using Telegram.Bot;
using Telegram.Bot.Args;
using LibWorkPlease;
using Telegram.Bot.Types.ReplyMarkups;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace LanBot
{
    public class Bot
    {
        public static async void Bot_OnMessage(object sender, MessageEventArgs mea)
        {
            var rep = new Repository();
            ReplyKeyboardMarkup rk;
            List<int> cTopic = new List<int>();
            long chatid = mea.Message.Chat.Id;
            ITelegramBotClient botClient = (ITelegramBotClient)sender;
            int topicid = 0;

            if (mea.Message.Text != null)
            {
                Console.WriteLine($"Message is received from {mea.Message.Chat.Username}: {mea.Message.Text}");
                var answer = mea.Message.Text;
                    Word w;
                    var userExists = rep.GetUser(chatid);
                    State curState = 0;
                if (userExists != null)
                {
                    curState = userExists.UState;
                    topicid = userExists.CurTopicID;
                }

                else
                {
                    rep.AddUser(chatid, State.Error, Infrastructure.GetNewWord().WordId);
                    userExists = rep.GetUser(chatid);
                }
                    if (answer == "Cancel")
                    {
                    rep.ChangeState(chatid, State.Start);
                        await Helpers.LearnCreateRKM(mea, botClient);
                        return;
                    }

                    w = rep.GetWord(userExists.CurrentWord);
                    if (answer == "/start")
                    {
                        await botClient.SendTextMessageAsync(
                                chatId: mea.Message.Chat,
                                text: File.ReadAllText("Instructions.txt"));
                        curState = 0;
                    }

                    switch (curState)
                    {
                        case (0):
                            curState++;
                            rk = await Helpers.LearnCreateRKM(mea, botClient);
                            break;

                        case (State.Start):
                            curState = Infrastructure.Start(chatid, answer);
                            if (curState == State.Screating)
                            {
                                await botClient.SendTextMessageAsync(
                                    chatId: mea.Message.Chat,
                                    text: "Type the name of your topic");
                            }
                            else if (curState == State.Slearning)
                            {
                                rk = Helpers.MakeRKM(mea, "Random", "From a topic");
                                await botClient.SendTextMessageAsync(
                                    chatId: mea.Message.Chat,
                                    text: "Do you want to learn words randomly or from a themed topic?",
                                    replyMarkup: rk);
                            }
                            
                            break;

                        case (State.Slearning):
                            curState = Infrastructure.ChooseWayOfLearning(chatid, answer);
                            rk = Helpers.MakeRKM(mea, "I don't know!", "Cancel");
                            if (curState == State.Learning)
                                await botClient.SendTextMessageAsync(
                                    chatId: mea.Message.Chat,
                                    text: $"The first word to translate is {w.Rus_version}",
                                    replyMarkup: rk);
                            else if (curState == State.ChoosingTopic)
                                await botClient.SendTextMessageAsync(
                                    chatId: mea.Message.Chat,
                                    text: "Please type the name of the chosen topic.");
                            break;

                        case (State.Learning):
                            await Helpers.WordCheckS(mea, chatid, botClient);
                            break;

                        case (State.LearningTopic):
                            await Helpers.WordCheck(mea, chatid, botClient);
                            break;

                        case (State.ChoosingTopic):
                            var txt = mea.Message.Text;
                            Topic topic = rep.FindTopic(txt);
                            if (topic != null)
                            {
                                curState = State.LearningTopic;
                                cTopic= Infrastructure.FindTopic(topic, chatid);
                                if (cTopic.Count == 0)
                                {
                                    curState = State.Start;
                                    userExists.CurTopic = null;
                                    await botClient.SendTextMessageAsync(
                                    chatId: mea.Message.Chat,
                                    text: $"The topic is empty");
                                    await Helpers.LearnCreateRKM(mea, botClient);
                                }
                                else
                                {
                                    var word = rep.GetWord(cTopic[cTopic.Count-1]);
                                    userExists.CurrentWord = cTopic[cTopic.Count-1];
                                    cTopic = cTopic.SkipLast(1).ToList();
                                    userExists.CurTopic = cTopic;
                                    userExists.CurTopicID = topic.TopicId;
                                    topicid = topic.TopicId;
                                    await botClient.SendTextMessageAsync(
                                        chatId: mea.Message.Chat,
                                        text: $"The first word to translate is {word.Rus_version}");
                                }
                            }
                            else
                            {
                                await botClient.SendTextMessageAsync(
                                    chatId: mea.Message.Chat,
                                    text: "The topic doesn't exist.");
                            }
                            break;

                        case (State.Screating):
                            curState = Infrastructure.CreateTopic(mea.Message.Text, mea.Message.Chat.Id);
                            if (curState == State.Screating)
                                await botClient.SendTextMessageAsync(
                                    chatId: mea.Message.Chat,
                                    text: "Such topic already exists");
                            else
                            {
                                rk = Helpers.MakeRKM(mea, "Cancel", "Done");
                                await botClient.SendTextMessageAsync(
                                    chatId: mea.Message.Chat,
                                    text: "Now let's add the words in your topic" +
                                    "\n Add a word in format \n" +
                                    "\"ENG_WORD RUS_WORD\" \n" +
                                    "Press Done button when you're done", replyMarkup: rk);
                            }
                            break;

                        case (State.Adding):

                            curState = Infrastructure.AddWord(mea.Message.Text, topicid);
                            if (curState == State.Created)
                            {
                                userExists.CurTopicID = topicid;
                            userExists.CurTopic = rep.CreateTopic(chatid, topicid);
                                rk = Helpers.MakeRKM(mea, "Yes", "No");
                                await botClient.SendTextMessageAsync(
                                    chatId: mea.Message.Chat,
                                    text: "Nailed it! Do you want to learn it?", replyMarkup: rk);
                            }
                            break;

                        case (State.Created):
                            if (mea.Message.Text == "No")
                            {
                                await Helpers.LearnCreateRKM(mea, botClient);
                                curState = 0;
                            }
                            else if (mea.Message.Text == "Yes")
                            {
                                curState = State.ChoosingTopic;
                                var wordid = userExists.CurTopic[userExists.CurTopic.Capacity-1];
                                userExists.CurrentWord = wordid;
                                await botClient.SendTextMessageAsync(
                                        chatId: mea.Message.Chat,
                                        text: $"The first word to translate is {rep.GetWord(wordid).Rus_version}");
                                curState = State.LearningTopic;
                            }
                            break;

                        default:
                            await botClient.SendTextMessageAsync(
                                chatId: mea.Message.Chat,
                                text: "Something went wrong... Press /start to return to the initial state");
                            break;
                    }
                rep.ChangeState(chatid, curState);
                Helpers.SendRequest(chatid, curState, mea.Message.Text);               
            }
        }        
    }
}
